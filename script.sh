#!/bin/bash

parse_dir () {
    local prefix="$1"
    local dirname="$2"
    local varname="$3"
    local paths=`find-paths.py $prefix $dirname`
    while read -r path
    do
            echo "export $varname=$path:\$$varname"
            eval "export $varname=$path:\$$varname"
    done <<< "$paths"
}