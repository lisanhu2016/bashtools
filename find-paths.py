#!/bin/python3

import argparse
import os

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('prefix')
    parser.add_argument('dirname')
    args = parser.parse_args()

    for root, dirs, files in os.walk(args.prefix):
        for dir in dirs:
            if dir == args.dirname:
                print(os.path.join(root, dir))
